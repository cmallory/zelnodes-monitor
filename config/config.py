from os import path

import jstyleson
from structlog import get_logger

logger = get_logger(__name__)


class Config:
    """
    Hold configuration values
    """
    def __init__(self):
        self.nodelist: list = []
        self.databasetype: str = ""
        self.dbaddress: str = ""
        self.dbport: str = ""
        self.dbusername: str = ""
        self.dbpassword: str = ""
        self.db: str = ""
        self.dbfile: str = ""
        self.discordenabled: bool = False
        self.discordwebhook: str = ""
        self.nodeadminid: int = 0

    def read_config(self):
        """
        Reads config file values into class
        :return:
        """
        # Reads config file as read-only
        try:
            with open(f"config{path.sep}config.json", "r") as configfile:
                conf = configfile.read()

            # Use jstyleson to handle comments in json
            confjson = jstyleson.loads(conf)
        except:
            logger.exception("Failed to read nodelist.json")
            return

        # Populate class values with ones read from file
        for node in confjson['ip_addrs']:
            self.nodelist.append(node)

        for setting in confjson["databases"]:
            self.databasetype = setting["databasetype"]
            self.dbaddress = setting["address"]
            self.dbport = str(setting["port"])
            self.dbusername = setting["username"]
            self.dbpassword = setting["password"]
            self.db = setting["database"]
            self.dbfile = setting["file"]

        for discord in confjson["discord"]:
            self.discordenabled = bool(discord["enabled"])
            self.discordwebhook = discord["webhookurl"]
            self.nodeadminid = int(discord["nodeadminid"])
