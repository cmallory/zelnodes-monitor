from dataclasses import dataclass

from packaging.version import parse
from structlog import get_logger

logger = get_logger(__name__)


@dataclass()
class DiscordAlerts:
    """
    Data class to hold properties for Discord alerts
    """
    nodeip: str = ""
    nodetier: str = ""
    nodestatus: str = ""
    benchstatus: str = ""
    zelcashversion: str = ""
    zelfluxversion: str = ""
    zelbenchversion: str = ""
    releasedzelcashversion: str = ""
    releasedzelfluxversion: str = ""
    releasedzelbenchversion: str = ""

    @staticmethod
    def version_compare(ver1: str, ver2: str) -> int:
        """
        Compares version numbers as str.
        0 = Same version
        1 = Update available
        -1 = Current version is newer than release
        :param ver1: Current version as str
        :param ver2: Release version as str
        :return: Int result
        """
        try:
            currentversion = parse(ver1)
            releaseversion = parse(ver2)

            if currentversion == releaseversion:
                return 0
            elif currentversion < releaseversion:
                return 1
            else:
                return -1
        except:
            logger.warn(f"Failed to compare versions: {ver1}, {ver2}")
            # Return 0 since comparison cannot happen - assume correct until next check
            return 0
