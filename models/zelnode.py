from sqlalchemy import Column, MetaData, Table
from sqlalchemy.dialects.postgresql import *
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import declarative_base
from structlog import get_logger

logger = get_logger(__name__)

# Declare separate bases for each DB class
Base_pg = declarative_base()
Base_sl = declarative_base()


def create_postgresql_tables(engine: Engine):
    """
    Creates Postgresql table if it does not yet exist
    :param engine:
    :return:
    """
    metadata = MetaData(engine)
    Table("zelnodes", metadata,
          Column("id", BIGINT, primary_key=True, autoincrement=True),
          Column("nodedata", JSON),
          Column("timestamp", TIMESTAMP)
          )
    metadata.create_all()


def create_sqlite_tables(engine: Engine):
    """
    Create Sqlite table if it does not yet exist
    :param engine:
    :return:
    """
    metadata = MetaData(engine)
    Table("zelnodes", metadata,
          Column("id", INTEGER, primary_key=True, autoincrement=True),
          Column("nodedata", TEXT),
          Column("timestamp", TIMESTAMP)
          )
    metadata.create_all()


class ZelNode_Postgresql(Base_pg):
    __tablename__ = "zelnodes"
    id = Column(BIGINT, primary_key=True, autoincrement=True)
    nodedata = Column(JSON)
    timestamp = Column(TIMESTAMP)

    @staticmethod
    def save_to_database(entitylist, session):
        try:
            session.add_all(entitylist)
            session.commit()
        except:
            session.rollback()
            logger.exception("Failed to save to database")
        finally:
            session.close()


class ZelNode_Sqlite(Base_sl):
    __tablename__ = "zelnodes"
    id = Column(INTEGER, primary_key=True, autoincrement=True)
    nodedata = Column(JSON)
    timestamp = Column(TIMESTAMP)

    @staticmethod
    def save_to_database(entitylist, session):
        try:
            session.add_all(entitylist)
            session.commit()
        except:
            session.rollback()
            logger.exception("Failed to save to database")
        finally:
            session.close()
