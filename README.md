# Zelnodes Monitor
* Version 1.1.4
* Created by: Chad Mallory

## Description
Zelnodes Monitor will retrieve public node data from ZelFlux given a list of IP addresses and check the status of each.
Depending on whether or not the node shows "Expired" or "Starting", a Discord webhook notification can be sent to alert the maintainer of the nodes.
The benchmark status is also checked and a Discord notification can be sent for "Toaster" status.
All node data is stored in either Postgresql or Sqlite for data trends and reporting.  Node data is stored in Json format for easy retrieval using separate Python modules or Discord bots.
Intended to create a scheduled task, or cron job, to collect data in whatever interval you specify if wanting to collect data for historical purposes and reporting.

## Requirements
* Python 3.8 (virtual environment recommended)
* Choice of Postgresql or local Sqlite file
* Install necessary modules using the requirements.txt (`pip install -r requirements.txt` preferrably inside a virtual environment)

## Usage
* All options are configurable in the config.json file located in the config folder.  Use the config-sample.json as a starting template and adjust your settings as necessary.
* Replace IP addresses with your list of nodes to monitor.
* Choose between Postgresql or Sqlite for database storage.  Postgresql requires host address, port, username, password, and database name.  Sqlite only requires filename and database name.  Use lowercase text when specifying the database type.
* Choose to enable Discord notification.  If enabled, provide a webhook URL for the destination channel.
* Provide the Discord user ID (ID number, not string) of the user who maintains the nodes and should receive the mention in Discord.
* Start application either manually or with a scheduled task by using your Python3.8 executable followed by zelnode_monitor.py
* *Note: if running from cron job, cd to the application directory, use the path to python3.8 in your virtual environment followed by the full path to the zelnode_monitor.py.  Example: `0 */1 0 0 0 cd /root/zelnode_monitor; /root/zelnode_monitor/.env/bin/python3.8 /root/zelnode_monitor/zelnode_monitor.py` will schedule the task to run at the first of each and every hour.*

## Disclaimer
*This project is still work in progress and may contain bugs, errors, and possible inaccurate data.  Author will not be held responsible for any loss or damage.  Use at your own descretion and view code if in doubt.*

## Changelog
* 1.1.4
  * Fixes
    * Fixed issue with Discord alerts attempting to be sent even when disabled in the config.json.
    * Fixed missing casts for config properties when reading from file.
* 1.1.3
  * Fixes
    * Fixed typo when comparing versions to get health status
* 1.1.2
  * Fixes
     * Fixed issue with case-sensitive string compare for node status.
     * Fixed successful version compare not reporting up-to-date when node has status other than "Confirmed".
     * Fixed issue of "Expired" nodes still not reporting due to missing tier in json.
   * Changes
     * Changed node statuses to uppercase.
* 1.1.1
  * Fixes
    * Fixed loop iterator in wrong spot
* 1.1.0
  * Fixes
    * Fixed wrong variable for node IP when it is offline.
  * Changes
    * Refactored Discord alerts into its own data class to allow for better handling and assignments.
  * New
    * Added versions for zelcash, zelflux, and zelbench to the Discord alert.
    * Added version comparisons to show if up-to-date or needs updating (will ping admin if update is needed).
    * Added benchmark status to Discord alerts
* 1.0.2
  * Fixes
    * Fixed leftover loop for Sqlite processing
* 1.0.1
  * Fixes
    * Fixed config file path issue by using OS specific path separator.
  * Changes
    * Reformatted Discord embeds by combining results into one embed.
  * New
    * Added Node Tier to Discord embed field.
    * Added missing node status of "Starting" to node check.
* 1.0.0 - Initial commit
